package academy.campus.anthony.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import academy.campus.anthony.IntegrationTest;
import academy.campus.anthony.domain.BookComment;
import academy.campus.anthony.repository.BookCommentRepository;
import academy.campus.anthony.service.BookCommentService;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BookCommentResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class BookCommentResourceIT {

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/book-comments";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BookCommentRepository bookCommentRepository;

    @Mock
    private BookCommentRepository bookCommentRepositoryMock;

    @Mock
    private BookCommentService bookCommentServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBookCommentMockMvc;

    private BookComment bookComment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookComment createEntity(EntityManager em) {
        BookComment bookComment = new BookComment().comment(DEFAULT_COMMENT);
        return bookComment;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BookComment createUpdatedEntity(EntityManager em) {
        BookComment bookComment = new BookComment().comment(UPDATED_COMMENT);
        return bookComment;
    }

    @BeforeEach
    public void initTest() {
        bookComment = createEntity(em);
    }

    @Test
    @Transactional
    void createBookComment() throws Exception {
        int databaseSizeBeforeCreate = bookCommentRepository.findAll().size();
        // Create the BookComment
        restBookCommentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bookComment)))
            .andExpect(status().isCreated());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeCreate + 1);
        BookComment testBookComment = bookCommentList.get(bookCommentList.size() - 1);
        assertThat(testBookComment.getComment()).isEqualTo(DEFAULT_COMMENT);
    }

    @Test
    @Transactional
    void createBookCommentWithExistingId() throws Exception {
        // Create the BookComment with an existing ID
        bookComment.setId(1L);

        int databaseSizeBeforeCreate = bookCommentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookCommentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bookComment)))
            .andExpect(status().isBadRequest());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCommentIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookCommentRepository.findAll().size();
        // set the field null
        bookComment.setComment(null);

        // Create the BookComment, which fails.

        restBookCommentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bookComment)))
            .andExpect(status().isBadRequest());

        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllBookComments() throws Exception {
        // Initialize the database
        bookCommentRepository.saveAndFlush(bookComment);

        // Get all the bookCommentList
        restBookCommentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bookComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllBookCommentsWithEagerRelationshipsIsEnabled() throws Exception {
        when(bookCommentServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restBookCommentMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(bookCommentServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllBookCommentsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(bookCommentServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restBookCommentMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(bookCommentServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getBookComment() throws Exception {
        // Initialize the database
        bookCommentRepository.saveAndFlush(bookComment);

        // Get the bookComment
        restBookCommentMockMvc
            .perform(get(ENTITY_API_URL_ID, bookComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bookComment.getId().intValue()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT));
    }

    @Test
    @Transactional
    void getNonExistingBookComment() throws Exception {
        // Get the bookComment
        restBookCommentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewBookComment() throws Exception {
        // Initialize the database
        bookCommentRepository.saveAndFlush(bookComment);

        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();

        // Update the bookComment
        BookComment updatedBookComment = bookCommentRepository.findById(bookComment.getId()).get();
        // Disconnect from session so that the updates on updatedBookComment are not directly saved in db
        em.detach(updatedBookComment);
        updatedBookComment.comment(UPDATED_COMMENT);

        restBookCommentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedBookComment.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedBookComment))
            )
            .andExpect(status().isOk());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
        BookComment testBookComment = bookCommentList.get(bookCommentList.size() - 1);
        assertThat(testBookComment.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void putNonExistingBookComment() throws Exception {
        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();
        bookComment.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookCommentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, bookComment.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bookComment))
            )
            .andExpect(status().isBadRequest());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBookComment() throws Exception {
        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();
        bookComment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBookCommentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(bookComment))
            )
            .andExpect(status().isBadRequest());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBookComment() throws Exception {
        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();
        bookComment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBookCommentMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(bookComment)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBookCommentWithPatch() throws Exception {
        // Initialize the database
        bookCommentRepository.saveAndFlush(bookComment);

        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();

        // Update the bookComment using partial update
        BookComment partialUpdatedBookComment = new BookComment();
        partialUpdatedBookComment.setId(bookComment.getId());

        partialUpdatedBookComment.comment(UPDATED_COMMENT);

        restBookCommentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBookComment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBookComment))
            )
            .andExpect(status().isOk());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
        BookComment testBookComment = bookCommentList.get(bookCommentList.size() - 1);
        assertThat(testBookComment.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void fullUpdateBookCommentWithPatch() throws Exception {
        // Initialize the database
        bookCommentRepository.saveAndFlush(bookComment);

        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();

        // Update the bookComment using partial update
        BookComment partialUpdatedBookComment = new BookComment();
        partialUpdatedBookComment.setId(bookComment.getId());

        partialUpdatedBookComment.comment(UPDATED_COMMENT);

        restBookCommentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBookComment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBookComment))
            )
            .andExpect(status().isOk());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
        BookComment testBookComment = bookCommentList.get(bookCommentList.size() - 1);
        assertThat(testBookComment.getComment()).isEqualTo(UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void patchNonExistingBookComment() throws Exception {
        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();
        bookComment.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookCommentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, bookComment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bookComment))
            )
            .andExpect(status().isBadRequest());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBookComment() throws Exception {
        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();
        bookComment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBookCommentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(bookComment))
            )
            .andExpect(status().isBadRequest());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBookComment() throws Exception {
        int databaseSizeBeforeUpdate = bookCommentRepository.findAll().size();
        bookComment.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBookCommentMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(bookComment))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the BookComment in the database
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBookComment() throws Exception {
        // Initialize the database
        bookCommentRepository.saveAndFlush(bookComment);

        int databaseSizeBeforeDelete = bookCommentRepository.findAll().size();

        // Delete the bookComment
        restBookCommentMockMvc
            .perform(delete(ENTITY_API_URL_ID, bookComment.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BookComment> bookCommentList = bookCommentRepository.findAll();
        assertThat(bookCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
