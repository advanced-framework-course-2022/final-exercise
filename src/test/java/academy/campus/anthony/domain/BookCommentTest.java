package academy.campus.anthony.domain;

import static org.assertj.core.api.Assertions.assertThat;

import academy.campus.anthony.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class BookCommentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookComment.class);
        BookComment bookComment1 = new BookComment();
        bookComment1.setId(1L);
        BookComment bookComment2 = new BookComment();
        bookComment2.setId(bookComment1.getId());
        assertThat(bookComment1).isEqualTo(bookComment2);
        bookComment2.setId(2L);
        assertThat(bookComment1).isNotEqualTo(bookComment2);
        bookComment1.setId(null);
        assertThat(bookComment1).isNotEqualTo(bookComment2);
    }
}
