import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBookComment } from '../book-comment.model';

@Component({
  selector: 'jhi-book-comment-detail',
  templateUrl: './book-comment-detail.component.html',
})
export class BookCommentDetailComponent implements OnInit {
  bookComment: IBookComment | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookComment }) => {
      this.bookComment = bookComment;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
