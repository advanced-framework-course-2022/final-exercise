import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BookCommentDetailComponent } from './book-comment-detail.component';

describe('BookComment Management Detail Component', () => {
  let comp: BookCommentDetailComponent;
  let fixture: ComponentFixture<BookCommentDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BookCommentDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ bookComment: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(BookCommentDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(BookCommentDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load bookComment on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.bookComment).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
