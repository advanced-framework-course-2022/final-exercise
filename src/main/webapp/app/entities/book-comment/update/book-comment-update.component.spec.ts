import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { BookCommentService } from '../service/book-comment.service';
import { IBookComment, BookComment } from '../book-comment.model';
import { IBook } from 'app/entities/book/book.model';
import { BookService } from 'app/entities/book/service/book.service';
import { IClient } from 'app/entities/client/client.model';
import { ClientService } from 'app/entities/client/service/client.service';

import { BookCommentUpdateComponent } from './book-comment-update.component';

describe('BookComment Management Update Component', () => {
  let comp: BookCommentUpdateComponent;
  let fixture: ComponentFixture<BookCommentUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let bookCommentService: BookCommentService;
  let bookService: BookService;
  let clientService: ClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [BookCommentUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BookCommentUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BookCommentUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    bookCommentService = TestBed.inject(BookCommentService);
    bookService = TestBed.inject(BookService);
    clientService = TestBed.inject(ClientService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call book query and add missing value', () => {
      const bookComment: IBookComment = { id: 456 };
      const book: IBook = { id: 29888 };
      bookComment.book = book;

      const bookCollection: IBook[] = [{ id: 65649 }];
      jest.spyOn(bookService, 'query').mockReturnValue(of(new HttpResponse({ body: bookCollection })));
      const expectedCollection: IBook[] = [book, ...bookCollection];
      jest.spyOn(bookService, 'addBookToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ bookComment });
      comp.ngOnInit();

      expect(bookService.query).toHaveBeenCalled();
      expect(bookService.addBookToCollectionIfMissing).toHaveBeenCalledWith(bookCollection, book);
      expect(comp.booksCollection).toEqual(expectedCollection);
    });

    it('Should call client query and add missing value', () => {
      const bookComment: IBookComment = { id: 456 };
      const client: IClient = { id: 51308 };
      bookComment.client = client;

      const clientCollection: IClient[] = [{ id: 39704 }];
      jest.spyOn(clientService, 'query').mockReturnValue(of(new HttpResponse({ body: clientCollection })));
      const expectedCollection: IClient[] = [client, ...clientCollection];
      jest.spyOn(clientService, 'addClientToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ bookComment });
      comp.ngOnInit();

      expect(clientService.query).toHaveBeenCalled();
      expect(clientService.addClientToCollectionIfMissing).toHaveBeenCalledWith(clientCollection, client);
      expect(comp.clientsCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const bookComment: IBookComment = { id: 456 };
      const book: IBook = { id: 4402 };
      bookComment.book = book;
      const client: IClient = { id: 47821 };
      bookComment.client = client;

      activatedRoute.data = of({ bookComment });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(bookComment));
      expect(comp.booksCollection).toContain(book);
      expect(comp.clientsCollection).toContain(client);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<BookComment>>();
      const bookComment = { id: 123 };
      jest.spyOn(bookCommentService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ bookComment });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: bookComment }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(bookCommentService.update).toHaveBeenCalledWith(bookComment);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<BookComment>>();
      const bookComment = new BookComment();
      jest.spyOn(bookCommentService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ bookComment });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: bookComment }));
      saveSubject.complete();

      // THEN
      expect(bookCommentService.create).toHaveBeenCalledWith(bookComment);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<BookComment>>();
      const bookComment = { id: 123 };
      jest.spyOn(bookCommentService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ bookComment });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(bookCommentService.update).toHaveBeenCalledWith(bookComment);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackBookById', () => {
      it('Should return tracked Book primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackBookById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackClientById', () => {
      it('Should return tracked Client primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackClientById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
