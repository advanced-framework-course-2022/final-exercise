import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IBookComment, BookComment } from '../book-comment.model';
import { BookCommentService } from '../service/book-comment.service';
import { IBook } from 'app/entities/book/book.model';
import { BookService } from 'app/entities/book/service/book.service';
import { IClient } from 'app/entities/client/client.model';
import { ClientService } from 'app/entities/client/service/client.service';

@Component({
  selector: 'jhi-book-comment-update',
  templateUrl: './book-comment-update.component.html',
})
export class BookCommentUpdateComponent implements OnInit {
  isSaving = false;

  booksCollection: IBook[] = [];
  clientsCollection: IClient[] = [];

  editForm = this.fb.group({
    id: [],
    comment: [null, [Validators.required, Validators.maxLength(200)]],
    book: [],
    client: [],
  });

  constructor(
    protected bookCommentService: BookCommentService,
    protected bookService: BookService,
    protected clientService: ClientService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bookComment }) => {
      this.updateForm(bookComment);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bookComment = this.createFromForm();
    if (bookComment.id !== undefined) {
      this.subscribeToSaveResponse(this.bookCommentService.update(bookComment));
    } else {
      this.subscribeToSaveResponse(this.bookCommentService.create(bookComment));
    }
  }

  trackBookById(_index: number, item: IBook): number {
    return item.id!;
  }

  trackClientById(_index: number, item: IClient): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBookComment>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(bookComment: IBookComment): void {
    this.editForm.patchValue({
      id: bookComment.id,
      comment: bookComment.comment,
      book: bookComment.book,
      client: bookComment.client,
    });

    this.booksCollection = this.bookService.addBookToCollectionIfMissing(this.booksCollection, bookComment.book);
    this.clientsCollection = this.clientService.addClientToCollectionIfMissing(this.clientsCollection, bookComment.client);
  }

  protected loadRelationshipsOptions(): void {
    this.bookService
      .query({ 'bookCommentId.specified': 'false' })
      .pipe(map((res: HttpResponse<IBook[]>) => res.body ?? []))
      .pipe(map((books: IBook[]) => this.bookService.addBookToCollectionIfMissing(books, this.editForm.get('book')!.value)))
      .subscribe((books: IBook[]) => (this.booksCollection = books));

    this.clientService
      .query({ 'bookCommentId.specified': 'false' })
      .pipe(map((res: HttpResponse<IClient[]>) => res.body ?? []))
      .pipe(map((clients: IClient[]) => this.clientService.addClientToCollectionIfMissing(clients, this.editForm.get('client')!.value)))
      .subscribe((clients: IClient[]) => (this.clientsCollection = clients));
  }

  protected createFromForm(): IBookComment {
    return {
      ...new BookComment(),
      id: this.editForm.get(['id'])!.value,
      comment: this.editForm.get(['comment'])!.value,
      book: this.editForm.get(['book'])!.value,
      client: this.editForm.get(['client'])!.value,
    };
  }
}
