import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IBookComment } from '../book-comment.model';
import { BookCommentService } from '../service/book-comment.service';

@Component({
  templateUrl: './book-comment-delete-dialog.component.html',
})
export class BookCommentDeleteDialogComponent {
  bookComment?: IBookComment;

  constructor(protected bookCommentService: BookCommentService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bookCommentService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
