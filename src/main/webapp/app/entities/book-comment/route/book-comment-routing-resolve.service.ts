import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBookComment, BookComment } from '../book-comment.model';
import { BookCommentService } from '../service/book-comment.service';

@Injectable({ providedIn: 'root' })
export class BookCommentRoutingResolveService implements Resolve<IBookComment> {
  constructor(protected service: BookCommentService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBookComment> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((bookComment: HttpResponse<BookComment>) => {
          if (bookComment.body) {
            return of(bookComment.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BookComment());
  }
}
