import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { BookCommentComponent } from '../list/book-comment.component';
import { BookCommentDetailComponent } from '../detail/book-comment-detail.component';
import { BookCommentUpdateComponent } from '../update/book-comment-update.component';
import { BookCommentRoutingResolveService } from './book-comment-routing-resolve.service';

const bookCommentRoute: Routes = [
  {
    path: '',
    component: BookCommentComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BookCommentDetailComponent,
    resolve: {
      bookComment: BookCommentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BookCommentUpdateComponent,
    resolve: {
      bookComment: BookCommentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BookCommentUpdateComponent,
    resolve: {
      bookComment: BookCommentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(bookCommentRoute)],
  exports: [RouterModule],
})
export class BookCommentRoutingModule {}
