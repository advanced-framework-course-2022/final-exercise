import { IBook } from 'app/entities/book/book.model';
import { IClient } from 'app/entities/client/client.model';

export interface IBookComment {
  id?: number;
  comment?: string;
  book?: IBook | null;
  client?: IClient | null;
}

export class BookComment implements IBookComment {
  constructor(public id?: number, public comment?: string, public book?: IBook | null, public client?: IClient | null) {}
}

export function getBookCommentIdentifier(bookComment: IBookComment): number | undefined {
  return bookComment.id;
}
