import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { BookCommentComponent } from './list/book-comment.component';
import { BookCommentDetailComponent } from './detail/book-comment-detail.component';
import { BookCommentUpdateComponent } from './update/book-comment-update.component';
import { BookCommentDeleteDialogComponent } from './delete/book-comment-delete-dialog.component';
import { BookCommentRoutingModule } from './route/book-comment-routing.module';

@NgModule({
  imports: [SharedModule, BookCommentRoutingModule],
  declarations: [BookCommentComponent, BookCommentDetailComponent, BookCommentUpdateComponent, BookCommentDeleteDialogComponent],
  entryComponents: [BookCommentDeleteDialogComponent],
})
export class BookCommentModule {}
