import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBookComment, getBookCommentIdentifier } from '../book-comment.model';

export type EntityResponseType = HttpResponse<IBookComment>;
export type EntityArrayResponseType = HttpResponse<IBookComment[]>;

@Injectable({ providedIn: 'root' })
export class BookCommentService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/book-comments');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(bookComment: IBookComment): Observable<EntityResponseType> {
    return this.http.post<IBookComment>(this.resourceUrl, bookComment, { observe: 'response' });
  }

  update(bookComment: IBookComment): Observable<EntityResponseType> {
    return this.http.put<IBookComment>(`${this.resourceUrl}/${getBookCommentIdentifier(bookComment) as number}`, bookComment, {
      observe: 'response',
    });
  }

  partialUpdate(bookComment: IBookComment): Observable<EntityResponseType> {
    return this.http.patch<IBookComment>(`${this.resourceUrl}/${getBookCommentIdentifier(bookComment) as number}`, bookComment, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBookComment>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBookComment[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addBookCommentToCollectionIfMissing(
    bookCommentCollection: IBookComment[],
    ...bookCommentsToCheck: (IBookComment | null | undefined)[]
  ): IBookComment[] {
    const bookComments: IBookComment[] = bookCommentsToCheck.filter(isPresent);
    if (bookComments.length > 0) {
      const bookCommentCollectionIdentifiers = bookCommentCollection.map(bookCommentItem => getBookCommentIdentifier(bookCommentItem)!);
      const bookCommentsToAdd = bookComments.filter(bookCommentItem => {
        const bookCommentIdentifier = getBookCommentIdentifier(bookCommentItem);
        if (bookCommentIdentifier == null || bookCommentCollectionIdentifiers.includes(bookCommentIdentifier)) {
          return false;
        }
        bookCommentCollectionIdentifiers.push(bookCommentIdentifier);
        return true;
      });
      return [...bookCommentsToAdd, ...bookCommentCollection];
    }
    return bookCommentCollection;
  }
}
