import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IBookComment, BookComment } from '../book-comment.model';

import { BookCommentService } from './book-comment.service';

describe('BookComment Service', () => {
  let service: BookCommentService;
  let httpMock: HttpTestingController;
  let elemDefault: IBookComment;
  let expectedResult: IBookComment | IBookComment[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(BookCommentService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      comment: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a BookComment', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new BookComment()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a BookComment', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          comment: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a BookComment', () => {
      const patchObject = Object.assign(
        {
          comment: 'BBBBBB',
        },
        new BookComment()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of BookComment', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          comment: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a BookComment', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addBookCommentToCollectionIfMissing', () => {
      it('should add a BookComment to an empty array', () => {
        const bookComment: IBookComment = { id: 123 };
        expectedResult = service.addBookCommentToCollectionIfMissing([], bookComment);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(bookComment);
      });

      it('should not add a BookComment to an array that contains it', () => {
        const bookComment: IBookComment = { id: 123 };
        const bookCommentCollection: IBookComment[] = [
          {
            ...bookComment,
          },
          { id: 456 },
        ];
        expectedResult = service.addBookCommentToCollectionIfMissing(bookCommentCollection, bookComment);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a BookComment to an array that doesn't contain it", () => {
        const bookComment: IBookComment = { id: 123 };
        const bookCommentCollection: IBookComment[] = [{ id: 456 }];
        expectedResult = service.addBookCommentToCollectionIfMissing(bookCommentCollection, bookComment);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(bookComment);
      });

      it('should add only unique BookComment to an array', () => {
        const bookCommentArray: IBookComment[] = [{ id: 123 }, { id: 456 }, { id: 84083 }];
        const bookCommentCollection: IBookComment[] = [{ id: 123 }];
        expectedResult = service.addBookCommentToCollectionIfMissing(bookCommentCollection, ...bookCommentArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const bookComment: IBookComment = { id: 123 };
        const bookComment2: IBookComment = { id: 456 };
        expectedResult = service.addBookCommentToCollectionIfMissing([], bookComment, bookComment2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(bookComment);
        expect(expectedResult).toContain(bookComment2);
      });

      it('should accept null and undefined values', () => {
        const bookComment: IBookComment = { id: 123 };
        expectedResult = service.addBookCommentToCollectionIfMissing([], null, bookComment, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(bookComment);
      });

      it('should return initial array if no BookComment is added', () => {
        const bookCommentCollection: IBookComment[] = [{ id: 123 }];
        expectedResult = service.addBookCommentToCollectionIfMissing(bookCommentCollection, undefined, null);
        expect(expectedResult).toEqual(bookCommentCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
