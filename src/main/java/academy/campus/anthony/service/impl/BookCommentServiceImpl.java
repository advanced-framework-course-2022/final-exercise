package academy.campus.anthony.service.impl;

import academy.campus.anthony.domain.BookComment;
import academy.campus.anthony.repository.BookCommentRepository;
import academy.campus.anthony.service.BookCommentService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link BookComment}.
 */
@Service
@Transactional
public class BookCommentServiceImpl implements BookCommentService {

    private final Logger log = LoggerFactory.getLogger(BookCommentServiceImpl.class);

    private final BookCommentRepository bookCommentRepository;

    public BookCommentServiceImpl(BookCommentRepository bookCommentRepository) {
        this.bookCommentRepository = bookCommentRepository;
    }

    @Override
    public BookComment save(BookComment bookComment) {
        log.debug("Request to save BookComment : {}", bookComment);
        return bookCommentRepository.save(bookComment);
    }

    @Override
    public BookComment update(BookComment bookComment) {
        log.debug("Request to save BookComment : {}", bookComment);
        return bookCommentRepository.save(bookComment);
    }

    @Override
    public Optional<BookComment> partialUpdate(BookComment bookComment) {
        log.debug("Request to partially update BookComment : {}", bookComment);

        return bookCommentRepository
            .findById(bookComment.getId())
            .map(existingBookComment -> {
                if (bookComment.getComment() != null) {
                    existingBookComment.setComment(bookComment.getComment());
                }

                return existingBookComment;
            })
            .map(bookCommentRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BookComment> findAll(Pageable pageable) {
        log.debug("Request to get all BookComments");
        return bookCommentRepository.findAll(pageable);
    }

    public Page<BookComment> findAllWithEagerRelationships(Pageable pageable) {
        return bookCommentRepository.findAllWithEagerRelationships(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BookComment> findOne(Long id) {
        log.debug("Request to get BookComment : {}", id);
        return bookCommentRepository.findOneWithEagerRelationships(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BookComment : {}", id);
        bookCommentRepository.deleteById(id);
    }
}
