package academy.campus.anthony.service;

import academy.campus.anthony.domain.BookComment;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link BookComment}.
 */
public interface BookCommentService {
    /**
     * Save a bookComment.
     *
     * @param bookComment the entity to save.
     * @return the persisted entity.
     */
    BookComment save(BookComment bookComment);

    /**
     * Updates a bookComment.
     *
     * @param bookComment the entity to update.
     * @return the persisted entity.
     */
    BookComment update(BookComment bookComment);

    /**
     * Partially updates a bookComment.
     *
     * @param bookComment the entity to update partially.
     * @return the persisted entity.
     */
    Optional<BookComment> partialUpdate(BookComment bookComment);

    /**
     * Get all the bookComments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BookComment> findAll(Pageable pageable);

    /**
     * Get all the bookComments with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BookComment> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" bookComment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BookComment> findOne(Long id);

    /**
     * Delete the "id" bookComment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
