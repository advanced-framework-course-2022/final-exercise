package academy.campus.anthony.web.rest;

import academy.campus.anthony.domain.BookComment;
import academy.campus.anthony.repository.BookCommentRepository;
import academy.campus.anthony.service.BookCommentService;
import academy.campus.anthony.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link academy.campus.anthony.domain.BookComment}.
 */
@RestController
@RequestMapping("/api")
public class BookCommentResource {

    private final Logger log = LoggerFactory.getLogger(BookCommentResource.class);

    private static final String ENTITY_NAME = "bookComment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BookCommentService bookCommentService;

    private final BookCommentRepository bookCommentRepository;

    public BookCommentResource(BookCommentService bookCommentService, BookCommentRepository bookCommentRepository) {
        this.bookCommentService = bookCommentService;
        this.bookCommentRepository = bookCommentRepository;
    }

    /**
     * {@code POST  /book-comments} : Create a new bookComment.
     *
     * @param bookComment the bookComment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bookComment, or with status {@code 400 (Bad Request)} if the bookComment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/book-comments")
    public ResponseEntity<BookComment> createBookComment(@Valid @RequestBody BookComment bookComment) throws URISyntaxException {
        log.debug("REST request to save BookComment : {}", bookComment);
        if (bookComment.getId() != null) {
            throw new BadRequestAlertException("A new bookComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BookComment result = bookCommentService.save(bookComment);
        return ResponseEntity
            .created(new URI("/api/book-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /book-comments/:id} : Updates an existing bookComment.
     *
     * @param id the id of the bookComment to save.
     * @param bookComment the bookComment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bookComment,
     * or with status {@code 400 (Bad Request)} if the bookComment is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bookComment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/book-comments/{id}")
    public ResponseEntity<BookComment> updateBookComment(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody BookComment bookComment
    ) throws URISyntaxException {
        log.debug("REST request to update BookComment : {}, {}", id, bookComment);
        if (bookComment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, bookComment.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!bookCommentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        BookComment result = bookCommentService.update(bookComment);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bookComment.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /book-comments/:id} : Partial updates given fields of an existing bookComment, field will ignore if it is null
     *
     * @param id the id of the bookComment to save.
     * @param bookComment the bookComment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bookComment,
     * or with status {@code 400 (Bad Request)} if the bookComment is not valid,
     * or with status {@code 404 (Not Found)} if the bookComment is not found,
     * or with status {@code 500 (Internal Server Error)} if the bookComment couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/book-comments/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<BookComment> partialUpdateBookComment(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody BookComment bookComment
    ) throws URISyntaxException {
        log.debug("REST request to partial update BookComment partially : {}, {}", id, bookComment);
        if (bookComment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, bookComment.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!bookCommentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BookComment> result = bookCommentService.partialUpdate(bookComment);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, bookComment.getId().toString())
        );
    }

    /**
     * {@code GET  /book-comments} : get all the bookComments.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bookComments in body.
     */
    @GetMapping("/book-comments")
    public ResponseEntity<List<BookComment>> getAllBookComments(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "true") boolean eagerload
    ) {
        log.debug("REST request to get a page of BookComments");
        Page<BookComment> page;
        if (eagerload) {
            page = bookCommentService.findAllWithEagerRelationships(pageable);
        } else {
            page = bookCommentService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /book-comments/:id} : get the "id" bookComment.
     *
     * @param id the id of the bookComment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bookComment, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/book-comments/{id}")
    public ResponseEntity<BookComment> getBookComment(@PathVariable Long id) {
        log.debug("REST request to get BookComment : {}", id);
        Optional<BookComment> bookComment = bookCommentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bookComment);
    }

    /**
     * {@code DELETE  /book-comments/:id} : delete the "id" bookComment.
     *
     * @param id the id of the bookComment to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/book-comments/{id}")
    public ResponseEntity<Void> deleteBookComment(@PathVariable Long id) {
        log.debug("REST request to delete BookComment : {}", id);
        bookCommentService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
