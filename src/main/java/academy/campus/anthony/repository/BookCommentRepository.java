package academy.campus.anthony.repository;

import academy.campus.anthony.domain.BookComment;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the BookComment entity.
 */
@Repository
public interface BookCommentRepository extends JpaRepository<BookComment, Long> {
    default Optional<BookComment> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<BookComment> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<BookComment> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct bookComment from BookComment bookComment left join fetch bookComment.book left join fetch bookComment.client",
        countQuery = "select count(distinct bookComment) from BookComment bookComment"
    )
    Page<BookComment> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct bookComment from BookComment bookComment left join fetch bookComment.book left join fetch bookComment.client")
    List<BookComment> findAllWithToOneRelationships();

    @Query(
        "select bookComment from BookComment bookComment left join fetch bookComment.book left join fetch bookComment.client where bookComment.id =:id"
    )
    Optional<BookComment> findOneWithToOneRelationships(@Param("id") Long id);
}
