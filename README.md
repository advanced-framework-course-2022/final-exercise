# Exercice final

## Développement

Installer les dépendances:

```
npm install
```

Lancer l'application:
```
./mvnw
npm start
```

## Démarches

### Ajout d'une entité au modèle existant

Modification du JDL par l'ajout des lignes suivantes:
```jdl
entity BookComment {
  comment String required maxlength(200)
}

relationship OneToOne {
  BookComment{book(name)} to Book
  BookComment{Client(email)} to Client
}

paginate Book, Client, Author, BookComment, BorrowedBook, Publisher with pagination
```

Execution du JDL avec la commande `jhipster jdl library.jdl`. JHipster n'écrase pas les fichiers déjà créés.

### Ajout d'une page "Aide"

#### Module

Création d'un module `HelpModule`:
```ts
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([HELP_ROUTE])
  ],
  declarations: [
    HelpComponent
  ],
})
export class HelpModule { }
```

- `RouterModule.forChild([HELP_ROUTE])` nous permet de déclarer des routes enfants (expliqué plus bas dans le document).
- Le composant `HelpComponent` est déclaré dans le module.

#### Composant

Création d'un composant `HelpComponent` au même niveau que le module.
Le composant est déclaré dans le `HelpModule`.

#### Routage

Dans le fichier `app-routing.module.ts` la configuration passée au `RouterModule` a été modifiée pour ajouter un chemin vers le `HelpModule`.

```ts
{
  path: 'help',
  loadChildren: () => import('./help/help.module').then(m => m.HelpModule),
},
```

L'utilisation d'un `import` du module permet de charger le module que lorsque la route sera appelée.
En effet, si on ouvre l'onglet "Réseau" dans la console développeur, on peut voir qu'un fichier `src_main_webapp_app_help_help_module_ts.js` n'est chargé qu'au moment où l'on accède à la route.

Cette mécanique permet d'éviter de charger inutilement tous les modules lors du premier chargement de l'application.
